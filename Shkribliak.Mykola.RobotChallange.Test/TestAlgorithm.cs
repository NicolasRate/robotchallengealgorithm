﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace Shkribliak.Mykola.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestInitialize]
        public void HollyMethodBefore()
        {
            Console.WriteLine("TestInitialize");
        }

        [TestMethod]
        public void TestIsRangeOccupiedByMyRobots()
        {
            Console.WriteLine("TestIsRangeOccupiedByMyRobots");

            var algorithm = new ShkribliakAlgorithm();

            var positionToCheck = new Position(10, 10);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot
            {
                Energy = 100,
                OwnerName = algorithm.Author,
                Position = new Position(8, 8)
            });
            
            Assert.IsTrue(algorithm.IsRangeOccupiedByMyRobots(positionToCheck, robots, 4));
        }
        
        [TestMethod]
        public void TestIsRangeNotOccupiedByMyRobots()
        {
            Console.WriteLine("TestIsRangeNotOccupiedByMyRobots");

            var algorithm = new ShkribliakAlgorithm();

            var positionToCheck = new Position(10, 10);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot
            {
                Energy = 100,
                OwnerName = "qwerty",
                Position = new Position(8, 8)
            });
            
            Assert.IsFalse(algorithm.IsRangeOccupiedByMyRobots(positionToCheck, robots, 4));
        }
        
        [TestMethod]
        public void TestFindNearestRobotWithMaxEnergy()
        {
            Console.WriteLine("TestFindNearestRobotWithMaxEnergy");
            
            var algorithm = new ShkribliakAlgorithm();
            var movingRobot = new Robot.Common.Robot
            {
                Energy = 100,
                OwnerName = algorithm.Author,
                Position = new Position(10, 10)
            };
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot
            {
                Energy = 2000,
                OwnerName = algorithm.Author,
                Position = new Position(8, 8)
            });
            robots.Add(new Robot.Common.Robot
            {
                Energy = 500,
                OwnerName = "qwerty",
                Position = new Position(9, 9)
            });
            robots.Add(new Robot.Common.Robot
            {
                Energy = 800,
                OwnerName = "qwerty",
                Position = new Position(11, 11)
                // potentiallyStolenEnergy = 218
            });
            robots.Add(new Robot.Common.Robot
            {
                Energy = 824,
                OwnerName = "qwerty",
                Position = new Position(12, 12)
                // potentiallyStolenEnergy = 219
            });
            int potentiallyStolenEnergy = 219;
            
            Assert.AreEqual(potentiallyStolenEnergy, algorithm.FindNearestRobotWithMaxEnergy(movingRobot, robots).Item2);
        }
        
        [TestCleanup]
        public void HollyMethodAfter()
        {
            Console.WriteLine("TestCleanup");
        }

    }
}
