﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;

namespace Shkribliak.Mykola.RobotChallange.Test
{
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestFindDistance()
        {
            var position1 = new Position(0, 0);
            var position2 = new Position(4, 4);
            Assert.AreEqual(DistanceHelper.FindDistance(position1, position2), 32);
        }
    }
}
