using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Shkribliak.Mykola.RobotChallange
{
    public class ShkribliakAlgorithm : IRobotAlgorithm
    {
        private static int _roundNumber = 0;

        public ShkribliakAlgorithm()
        {
            Logger.OnLogRound += (sender, args) => _roundNumber++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];

            if (movingRobot.Energy >= 1100 && robots.Count(robot => robot.OwnerName == Author) < 100)
            {
                return new CreateNewRobotCommand();
            }

            (Robot.Common.Robot, int) robotToAttackInformation = FindNearestRobotWithMaxEnergy(movingRobot, robots);
            Robot.Common.Robot nextRobot = robotToAttackInformation.Item1;
            int clearEnergyFromRobot = robotToAttackInformation.Item2;

            Position nextPosition = FindPositionWithMaxEnergyToCollect(movingRobot, map, robots);
            if (nextPosition == null)
                return null;

            int energyFromStations = map.GetNearbyResources(nextPosition, 4).Sum(station => station.Energy);

            int distance = DistanceHelper.FindDistance(movingRobot.Position, nextPosition);

            int clearEnergyFromStations = energyFromStations - distance;

            if (_roundNumber < 30)
            {
                int energyNow = map.GetNearbyResources(movingRobot.Position, 4).Sum(station => station.Energy);
                int energyThen = clearEnergyFromStations;
                if (nextRobot != null && clearEnergyFromRobot >= clearEnergyFromStations)
                {
                    nextPosition = nextRobot.Position;
                    energyThen = clearEnergyFromRobot;
                }


                if (energyNow >= energyThen)
                {
                    return new CollectEnergyCommand();
                }

                return new MoveCommand
                {
                    NewPosition = nextPosition
                };
            }
            else
            {
                int energyNow = map.GetNearbyResources(movingRobot.Position, 4).Sum(station => station.Energy);
                int energyThen = clearEnergyFromStations;
                if (nextRobot != null && clearEnergyFromRobot >= distance)
                {
                    nextPosition = nextRobot.Position;
                    energyThen = clearEnergyFromRobot;
                }

                if (energyNow >= energyThen)
                {
                    return new CollectEnergyCommand();
                }

                return new MoveCommand
                {
                    NewPosition = nextPosition
                };
            }
        }

        public Position FindPositionWithMaxEnergyToCollect(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            int maxEnergy = 0;
            Position nextPosition = null;
            for (int x = 0; x < 100; x++)
            {
                for (int y = 0; y < 100; y++)
                {
                    Position pos = new Position(x, y);
                    if (IsRangeOccupiedByMyRobots(pos, robots, 4)) continue;
                    int energy = map.GetNearbyResources(pos, 4).Sum(station => station.Energy);
                    int distance = DistanceHelper.FindDistance(movingRobot.Position, pos);
                    if (energy > maxEnergy && energy - distance >= distance * 2 && movingRobot.Energy > distance * 1.35)
                    {
                        maxEnergy = energy;
                        nextPosition = pos;
                    }
                }
            }

            if (nextPosition == null)
            {
                nextPosition = movingRobot.Position;
            }

            return nextPosition;
        }

        public (Robot.Common.Robot, int) FindNearestRobotWithMaxEnergy(Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            int maxClearEnergy = 0;
            Robot.Common.Robot nextRobot = null;
            foreach (var robot in robots)
            {
                if (robot.OwnerName == Author) continue;

                Position pos = robot.Position;

                int dirtyEnergy = (int) (robot.Energy * 0.3);
                int distance = DistanceHelper.FindDistance(movingRobot.Position, pos);

                int clearEnergy = dirtyEnergy - distance - 20;


                if (clearEnergy > maxClearEnergy && movingRobot.Energy >= distance + 20)
                {
                    maxClearEnergy = clearEnergy;
                    nextRobot = robot;
                }
            }

            return (nextRobot, maxClearEnergy);
        }

        public bool IsRangeOccupiedByMyRobots(Position cell, IList<Robot.Common.Robot> robots,
            int distance)
        {
            return robots.Any(robot => (Math.Abs(robot.Position.X - cell.X) <= distance)
                                       && (Math.Abs(robot.Position.Y - cell.Y) <= distance)
                                       && (robot.OwnerName == Author));
        }

        public string Author
        {
            get { return "Shkribliak Mykola"; }
        }
    }
}